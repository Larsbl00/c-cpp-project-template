# C/CPP Project Template
A simple template for most C / CPP projects.
The template provides the following things
- Simple CMakeLists
- Conan.txt
- Visual Studio Code Config
- Clang Format file 
- Doxyfile, provides an override for the default doxyfile *that should be present*

## Requirements
| Requirement                                                 | Purpose                               |
| :---------------------------------------------------------- | :------------------------------------ |
| [Cmake](https://cmake.org/install/)                         | Used for cross-platform compiling     |
| [Conan](https://docs.conan.io/en/latest/installation.html)  | A C/CPP package manager               |
| [Doxygen](http://www.doxygen.nl/manual/install.html)        | Used for generating the documentation |
| [ClangFormat](https://clang.llvm.org/docs/ClangFormat.html) | Used for formatting code              |

## Project Structure
The desired project structure is similar to the one in [this](https://hiltmon.com/blog/2013/07/03/a-simple-c-plus-plus-project-structure/) article.

| Directory | Purpose                                                                                                                                                            |
| :-------- | :----------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| bin       | All app outputs, i.e. executables for the application and the tests                                                                                                |
| build     | Contains all object files, i.e. `*.o`                                                                                                                              |
| cmake     | Contains extra `.cmake` files, like modules                                                                                                                        |
| doc       | Contains the project documentation, you might want to add sub-directories if there is a lot. Also contains the [Doxyfile.in][doxyfile], which you should not touch |
| include   | Contains *all inclusions*, including third-party header files.                                                                                                     |
| lib       | Contains all library sources, i.e. `.so`, `.dll`, etc.                                                                                                             |
| licenses  | Contains the licenses of all libraries                                                                                                                             |
| src       | Contains all application sources, i.e. your `*.c` / `*.cpp` files                                                                                                  |
| test      | Contains your test files                                                                                                                                           |

> Note: If you add a namespace, add it to its own directory in the `include` and `src` locations.\
> Note: When a `.cpp` file has been placed in a sub-directory, add the directory to [Cmake][cmake], 
do not forget to provide the subdirectory with a `CMakeLists.txt` as well

### Structure and CMake
Every sub-directory present in `src` must contain a `CMakeLists.txt` and must be placed in a namespace of a similar name,
e.g. in `src/Foo/Foo.cpp` the class is placed inside a namespace called `foo`.

In the `.cpp` the headers need to be included using angled brackets (\</\>), 
not doing so causes the compiler some issues. 

Each `CMakeLists.txt` is responsible for the directory in which it is placed, it should also add all child directories.
Within `CMakeLists.txt`, one must first call `add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR/<DIR>})`.
After this one must append the sources of the directory to CMake sources.
This can be achieved as follows:
```cmake
# Add the foo sub-directory (Contains another CMakeLists.txt)
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Foo)

# Append the new sources
set(SOURCES
    ${SOURCES}                              # Copy previously present sources
    ${CMAKE_CURRENT_SOURCE_DIR}/<file>      # Append a source file
    ${CMAKE_CURRENT_SOURCE_DIR}/<file2>     # Append another file

    PARENT_SCOPE                            # Update the new sources to the parent, without it, it will ruin your day :D 
)
```

## Instructions

### VSC Settings
#### Editing
The [settings][vsc_settings] file contains all visual studio code settings for the project specifically.
When this file is altered it changes how the project is handled for others.
*Do* make sure that when you alter any settings for personal reasons, you keep them local.

#### Style
If you want to change the standard styling for the project, 
make sure to change it in the [settings][vsc_settings] file of the project.

### Installing Libraries
This project currently uses a `conanfile.txt`, 
if you want to add libraries add them below the `[requires]` section in the file.

To learn more about adding libraries using conan, read the [documentation](https://docs.conan.io/en/latest/reference/conanfile_txt.html).

Within the conanfile, you can also find a section `[options]`,
within this section you can configure certain things for libraries.
Please refer to the [conan option documentation](https://docs.conan.io/en/latest/using_packages/conanfile_txt.html#options-txt)

If you want to know more about the options you can use per library, 
read the documentation of the library developer.

### Commenting
In order to properly generate the [documentation](#documentation),
you are required to use the [Javadoc](https://www.tutorialspoint.com/java/java_documentation.htm) commenting style.

Every class, function, namespace, file, etc. needs to be commented using the aforementioned format.
> Tip: Download a commenting extension for your editor

### Documentation
The [cmakefile][cmake] has a custom target named `doc`, 
this will generate an `html` directory within the `doc` directory.
In the `html` dir, you will find a lot of different `.html` files,
look for the file named `index.html` and open it with your web browser.
You have now generated your documentation succesfully.

> Note: Before you can generate the documentation, make sure a `Doxyfile` is present in the project root.

### Running The Program
A couple of steps are required before you can build the program!

1. Create the [build](#project-structure) directory.
2. Enter the build directory, using the following command `cd build`.
3. Call the CMake file from within the build directory, using the following command `cmake .. -DCMAKE_BUILD_TYPE=<Debug|Release>`.
4. Based on your platform it will either output a `.sln` file (Windows) or a `Makefile` (MacOs / Linux).
5. Use the executable created by CMake, per default it should be stored in `bin`; however, check the [cmakefile][cmake] to be sure.

[cmake]: CMakeLists.txt
[doxyfile]: doc/Doxyfile.in
[vsc_settings]: .vscode/settings.json
