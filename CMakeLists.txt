cmake_minimum_required(VERSION 3.10)

# Create project
project(
    ProjectName
    VERSION 0.1
    LANGUAGES CXX C
)

# Language info
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_C_STANDARD 11)

# Set the outputs to the correct directories
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin)

########################
# Build Info
########################

#############
# Add flags #
if(MSVC)
    # Set for windows
    add_compile_options(/W4 /WX)
else()
    # Set for everything that is not windows
    add_compile_options(
        -Wall                   # Enable all warnings
        -Wextra                 # Enable extra warnings
        -pedantic               # Issue all the mandatory diagnostics listed in the C standard
        -Werror                 # Convert warnings to errors
        -Weffc++                # Warn users when they violate Scott Meyers' Effective C++ guidelines   
        -Wnon-virtual-dtor      # Warn user when class has virtual functions, yet lacks virtual destructor
        -Wctor-dtor-privacy     # Warn te user when all constructors / destructors canont be accessed
        -Woverloaded-virtual    # Warn user when a virtual function is overloaded instead of overridden
        -Wreorder               # Warn user when the order of attributes does not match the order of initialization
        -Wmissing-include-dirs  # Warn user when the included directory does not exist
    )
endif()

# Add build type flag
add_compile_options(-DBUILD_TYPE=${CMAKE_BUILD_TYPE})

###################
# Add module path #
set(CMAKE_MODULE_PATH "${CMAKE_MODULE_PATH};${CMAKE_SOURCE_DIR}/cmake/modules")

####################
# Libs and Sources #
####################

# Add cmake modules
include(${CMAKE_MODULE_PATH}/Doxygen.cmake)
include(${CMAKE_MODULE_PATH}/Conan.cmake)

# Files to include
# You will most likely not need to change this
include_directories(${CMAKE_SOURCE_DIR}/include)

# Source files of the project
# Add every sub-directory containing the cpp files
# Every sub-directory MUST contain another CmakeLists.txt
add_subdirectory(${CMAKE_SOURCE_DIR}/src)
