# Downloads all deps from the given conanfile (i.e. conanfile.txt, conanfile.py) autoamtically

if(NOT EXISTS "${CMAKE_BINARY_DIR}/conan.cmake")
   message(STATUS "Downloading conan.cmake from https://github.com/conan-io/cmake-conan")
   file(DOWNLOAD "https://raw.githubusercontent.com/conan-io/cmake-conan/master/conan.cmake"
                  "${CMAKE_BINARY_DIR}/conan.cmake")
endif()

include(${CMAKE_BINARY_DIR}/conan.cmake)

# Add remote hosts for a project
# conan_add_remote(NAME bincrafters INDEX 1
#                 URL https://api.bintray.com/conan/bincrafters/public-conan)

set(CONAN_CMAKE_SILENT_OUTPUT TRUE)

# Set platform specific conan settings (if needed)
if (MSVC)
    set(CONAN_SETTINGS
    
    )
else()
    set(CONAN_SETTINGS  
        compiler.libcxx=libstdc++
    )
endif()

# Run conan
conan_cmake_run(
    SETTINGS ${CONAN_SETTINGS}

    CONANFILE conanfile.txt     # Link to conanfile in project root
    BASIC_SETUP NO_OUTPUT_DIRS  # Added parameter to prevent overriding of the output dirs
)