option(BUILD_DOC "Generates the documentation" ON)

# Find package
find_package(Doxygen)
if (DOXYGEN_FOUND)
    # Config doxygen 
    set(DOXYGEN_IN  ${CMAKE_SOURCE_DIR}/doc/Doxyfile.in)
    set(DOXYGEN_OUT ${CMAKE_SOURCE_DIR}/Doxyfile)
    configure_file(${DOXYGEN_IN} ${DOXYGEN_OUT} @ONLY)

    message("Doxygen build started")
    # Add target
    add_custom_target(doc 
        COMMAND ${DOXYGEN_EXECUTABLE} ${DOXYGEN_OUT}
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        VERBATIM   
    )
else(DOXYGEN_FOUND)
    message("Could not find DOXYGEN, please make sure it is installed correctly")
endif(DOXYGEN_FOUND)